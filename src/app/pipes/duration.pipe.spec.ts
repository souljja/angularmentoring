import { DurationPipe } from './duration.pipe';

describe('DurationPipe', () => {
  let pipe: DurationPipe;
  beforeEach(() => {
    pipe = new DurationPipe();
  });
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  describe('check transform tethod', () => {
    it ('should return "2 min"', () => {
      expect(pipe.transform(2)).toBe('2 min');
    });

    it ('should return "1 h"', () => {
      expect(pipe.transform(60)).toBe('1 h');
    });

    it ('should return "1 h 23 min"', () => {
      expect(pipe.transform(83)).toBe('1 h 23 min');
    });
  });
});
