import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(value: number): string {
    const minutes = value % 60;
    if (value < 60) {
      return `${minutes} min`;
    } else {
      const hours = (value - value % 60) / 60;
      return minutes ? `${hours} h ${minutes} min` : `${hours} h`;
    }
  }
}
