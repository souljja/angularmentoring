import { CoursesToolboxModule } from './courses-toolbox.module';

describe('CoursesToolboxModule', () => {
  let coursesToolboxModule: CoursesToolboxModule;

  beforeEach(() => {
    coursesToolboxModule = new CoursesToolboxModule();
  });

  it('should create an instance', () => {
    expect(coursesToolboxModule).toBeTruthy();
  });
});
