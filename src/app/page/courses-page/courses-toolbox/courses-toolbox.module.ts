import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesToolboxComponent } from './courses-toolbox.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatIconModule
  ],
  declarations: [
    CoursesToolboxComponent
  ],
  exports: [
    CoursesToolboxComponent
  ]
})
export class CoursesToolboxModule { }
