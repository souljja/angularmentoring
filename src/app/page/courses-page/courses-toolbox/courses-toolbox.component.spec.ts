import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesToolboxComponent } from './courses-toolbox.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../../../shared/shared.module';

describe('CoursesToolboxComponent', () => {
  let component: CoursesToolboxComponent;
  let fixture: ComponentFixture<CoursesToolboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesToolboxComponent ],
      imports: [
        FormsModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesToolboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
