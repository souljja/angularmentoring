import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-courses-toolbox',
  templateUrl: './courses-toolbox.component.html',
  styleUrls: ['./courses-toolbox.component.css']
})
export class CoursesToolboxComponent implements OnInit {

  @Output()
  private searchClick = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  onClick(event: string): void {
    this.searchClick.emit(event);
  }
}
