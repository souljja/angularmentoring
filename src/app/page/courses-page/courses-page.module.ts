import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesPageComponent } from './courses-page.component';
import { CoreModule } from '../../core/core.module';
import { ListModule } from '../../list/list.module';
import { SharedModule } from '../../shared/shared.module';
import { CoursesToolboxModule } from './courses-toolbox/courses-toolbox.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    CoursesToolboxModule,
    ListModule
  ],
  declarations: [
    CoursesPageComponent
  ],
  exports: [
    CoursesPageComponent
  ]
})
export class CoursesPageModule { }
