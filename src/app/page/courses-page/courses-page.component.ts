import { Component, OnInit } from '@angular/core';
import { User } from '../../interfaces/user';
import { ListItem } from '../../interfaces/list-item';
import { ListService } from '../../list/list.service';

@Component({
  selector: 'app-courses-page',
  templateUrl: './courses-page.component.html',
  styleUrls: ['./courses-page.component.css']
})
export class CoursesPageComponent implements OnInit {

  private user: User = {
    id: '0',
    firstName: 'root',
    lastName: 'root'
  };

  private courses: Array<ListItem> = [];
  private allCourses: Array<ListItem> = [];

  constructor(private readonly listService: ListService) {
    this.allCourses = listService.getItems();
    this.courses = this.allCourses;
  }

  ngOnInit() {
  }

  public onSearchClick(event: string): void {
    this.courses = this.allCourses.filter((item: ListItem) => {
      return item.title.toUpperCase().includes(event.toUpperCase());
    });
  }
}
