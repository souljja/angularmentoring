import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesPageComponent } from './courses-page.component';
import { CoreModule } from '../../core/core.module';
import { BreadcrumbsModule } from '../../breadcrumbs/breadcrumbs.module';
import { ListModule } from '../../list/list.module';
import { CoursesToolboxModule } from './courses-toolbox/courses-toolbox.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('CoursesPageComponent', () => {
  let component: CoursesPageComponent;
  let fixture: ComponentFixture<CoursesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesPageComponent ],
      imports: [
        CoreModule,
        BreadcrumbsModule,
        ListModule,
        CoursesToolboxModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
