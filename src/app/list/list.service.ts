import { Injectable } from '@angular/core';
import { ListItem } from '../interfaces/list-item';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor() { }

  getItems(): ListItem[] {
    return [
      {
        id: '0',
        title: 'Title',
        topRated: false,
        creationDate: new Date('10/02/2011'),
        duration: 123,
        description: 'some too'
      },
      {
        id: '1',
        title: 'Title',
        topRated: true,
        creationDate: new Date('10/02/2019'),
        duration: 2,
        description: 'some too long text for testing of description'
      },
      {
        id: '2',
        title: 'Title',
        topRated: false,
        creationDate: new Date('07/15/2018'),
        duration: 540,
        description: 'some too long text for testing of description'
      },
      {
        id: '3',
        title: 'Some Amazing videocourse',
        topRated: false,
        creationDate: new Date('08/08/2018'),
        duration: 1440,
        description: 'some too long text for testing of description'
      }
    ];
  }
}
