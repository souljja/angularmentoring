import { Component, OnInit, Input } from '@angular/core';
import { ListItem } from '../interfaces/list-item';
import { ListService } from './list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public readonly NO_DATA_MESSAGE = 'No data. Feel free to add new course.';
  @Input() items: ListItem[] = [];

  constructor() {
  }

  ngOnInit() {
  }

  onDelete(item: ListItem): void {
    console.log(item.id);
  }

  onLoadMore(): void {
    console.log('Load more');
  }

  public itemsAvailable() {
    return this.items.length > 0;
  }

}
