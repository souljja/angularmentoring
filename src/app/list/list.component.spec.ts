import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { By } from '@angular/platform-browser';
import { ListItemComponent } from './list-item/list-item.component';
import { ListComponent } from './list.component';
import { SharedModule } from '../shared/shared.module';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListComponent, ListItemComponent],
      imports: [
        MatButtonModule,
        MatIconModule,
        SharedModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be 3 items', () => {
    const itemsMock = [
      {
        id: '0',
        title: 'some title',
        topRated: false,
        creationDate: new Date('10/02/1967'),
        duration: 2,
        description: ''
      },
      {
        id: '1',
        title: 'some title',
        topRated: false,
        creationDate: new Date('10/02/1967'),
        duration: 2,
        description: ''
      },
      {
        id: '2',
        title: 'some title',
        topRated: false,
        creationDate: new Date('10/02/1967'),
        duration: 2,
        description: ''
      }
    ];
    component.items = itemsMock;
    const items = fixture.debugElement.queryAll(By.css('.list-item-container'));
    expect(itemsMock.length).toBe(items.length);
  });
});
