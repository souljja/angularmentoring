import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemComponent } from './list-item.component';
import { By } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../../shared/shared.module';


describe('ListItemComponent', () => {
  let component: ListItemComponent;
  let fixture: ComponentFixture<ListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListItemComponent],
      imports: [
        MatButtonModule,
        MatIconModule,
        SharedModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onDelete method', () => {
    spyOn(component, 'onDelete');
    const button = fixture.debugElement.queryAll(By.css('.mat-raised-button.mat-accent'))[1];
    button.triggerEventHandler('click', null);

    expect(component.onDelete).toHaveBeenCalled();
  });

  it('should be formatted date', () => {
    component.item = {
      id: '0',
      title: 'some title',
      topRated: false,
      creationDate: new Date('10/02/1967'),
      duration: 2,
      description: ''
    };
    fixture.detectChanges();
    const item = fixture.debugElement.query(By.css('.list-item-date'));
    expect(item.nativeElement.textContent).toEqual(' 02.10.1967 ');
  });
});
