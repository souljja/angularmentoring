import { Component } from '@angular/core';
import { ListItem } from '../../interfaces/list-item';
import { ListItemComponent } from './list-item.component';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../../shared/shared.module';

@Component({
    template: `
      <app-list-item
        [item]="item" (delete)="onDelete($event)">
      </app-list-item>`
})
class TestHostComponent {
    public item: ListItem = {
        id: '0',
        title: 'some title',
        topRated: false,
        creationDate: new Date('10/02/1967'),
        duration: 2,
        description: ''
    };
    public deletedItem: ListItem;
    public onDelete(deletedItem: ListItem) { this.deletedItem = deletedItem; }
}

describe('ListItemComponentTestHost', () => {
    let component: TestHostComponent;
    let fixture: ComponentFixture<TestHostComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ListItemComponent, TestHostComponent],
            imports: [
                MatButtonModule,
                MatIconModule,
                SharedModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should receive correct deleted item', () => {
        const button = fixture.debugElement.queryAll(By.css('.mat-raised-button.mat-accent'))[1];
        button.triggerEventHandler('click', null);

        expect(component.deletedItem).toEqual(component.item);
    });
});
