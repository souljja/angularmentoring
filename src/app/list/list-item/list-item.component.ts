import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ListItem } from '../../interfaces/list-item';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  @Input()
  public item: ListItem;
  @Output()
  public delete = new EventEmitter<ListItem>();

  constructor() { }

  ngOnInit() {
  }

  onDelete(deletedItem: ListItem): void {
    this.delete.emit(deletedItem);
  }
}
