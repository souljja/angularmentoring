export interface ListItem {
    id: string;
    title: string;
    topRated: boolean;
    creationDate: Date;
    duration: number;
    description: string;
}
