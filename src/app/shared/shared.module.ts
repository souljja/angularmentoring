import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbsModule } from '../breadcrumbs/breadcrumbs.module';
import { SearchBoxModule } from '../search-box/search-box.module';
import { TenseModule } from '../tense/tense.module';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
    imports: [
        CommonModule,
        BreadcrumbsModule,
        SearchBoxModule,
        TenseModule,
        PipesModule
    ],
    declarations: [],
    exports: [
        BreadcrumbsModule,
        SearchBoxModule,
        TenseModule,
        PipesModule
    ]
})
export class SharedModule { }
