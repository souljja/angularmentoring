import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../interfaces/user';


@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.css']
})
export class MainHeaderComponent implements OnInit {
  @Input()
  public user: User;

  constructor() { }

  ngOnInit() {
  }

}
