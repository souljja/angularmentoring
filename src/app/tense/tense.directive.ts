import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appTense]'
})
export class TenseDirective implements OnInit {
  private static TWO_WEEKS = 12096e5;
  private static MATERIAL_GREEN = '#69F0AE';
  private static MATERIAL_BLUE = '#3f51b5';
  @Input()
  public creationDate: Date;

  constructor(private element: ElementRef, private readonly renderer: Renderer2) { }

  ngOnInit() {
    this.setBorderColor(this.creationDate);
  }

  private setBorderColor(creationDate: Date): void {
    const currentDate = new Date();
    const twoWeeksAgo = new Date(currentDate.getTime() - TenseDirective.TWO_WEEKS);
    if (creationDate < currentDate && creationDate >= twoWeeksAgo) {
      this.setStyle('border', `2px solid ${ TenseDirective.MATERIAL_GREEN }`);
    } else if (creationDate > currentDate) {
      this.setStyle('border', `2px solid ${ TenseDirective.MATERIAL_BLUE }`);
    }
  }

  private setStyle(style: string, value: string): void {
    this.renderer.setStyle(this.element.nativeElement, style, value);
  }

}
