import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TenseDirective } from './tense.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TenseDirective
  ],
  exports: [
    TenseDirective
  ]
})
export class TenseModule { }
