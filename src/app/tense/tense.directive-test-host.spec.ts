import { Component, DebugElement } from '../../../node_modules/@angular/core';
import { TestBed, ComponentFixture } from '../../../node_modules/@angular/core/testing';
import { By } from '../../../node_modules/@angular/platform-browser';
import { TenseDirective } from './tense.directive';

@Component({
  template: `<div [creationDate]="creationDate" appTense>`
})
class TestTenseDirectiveComponent {
  public creationDate: Date = new Date();
}

describe('TenseDirective', () => {

  let component: TestTenseDirectiveComponent;
  let fixture: ComponentFixture<TestTenseDirectiveComponent>;
  let element: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestTenseDirectiveComponent, TenseDirective]
    });
    fixture = TestBed.createComponent(TestTenseDirectiveComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement.query(By.css('div'));
  });

  describe('check border-color', () => {
    it ('should have default border-color', () => {
      expect(element.nativeElement.style.borderColor).toBe('');
    });

    it ('should be green for 14 days ago dates', () => {
      const currentDate = new Date();
      component.creationDate = new Date(currentDate.getTime() - 1000);
      fixture.detectChanges();

      expect(element.nativeElement.style.borderColor).toBe('rgb(105, 240, 174)');
    });

    it ('should be blue for future date', () => {
      const currentDate = new Date();
      component.creationDate = new Date(currentDate.getTime() + 5000);
      fixture.detectChanges();

      expect(element.nativeElement.style.borderColor).toBe('rgb(63, 81, 181)');
    });
  });
});
