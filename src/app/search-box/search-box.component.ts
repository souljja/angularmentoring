import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit {
  private searchInput = '';
  @Output()
  public searchClick = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onClick(): void {
    this.searchClick.emit(this.searchInput);
  }
}
